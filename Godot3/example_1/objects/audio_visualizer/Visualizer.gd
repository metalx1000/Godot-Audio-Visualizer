extends Control

#This script takes the frequencies have an audio bus and displays the values
# on the screen in text form

export var COUNT = 20 #number of output values
export var FREQ_MAX = 10000.0 # 21050.0

#get second audio bus / first effect
var spectrum = AudioServer.get_bus_effect_instance(1,0)


func _process(_delta):
	var prev_hz = 0
	var magnitude_full_db = spectrum.get_magnitude_for_frequency_range(0, 1500).length()

	var list = ""
	for i in range(COUNT):	
		var hz = (i + 1) * FREQ_MAX / COUNT;
		var magnitude: float = spectrum.get_magnitude_for_frequency_range(prev_hz, hz).length()
		list += str(magnitude) + "\n"

		prev_hz = hz

	$output.text = list




